export const environment = {
  production: true,
  base_uri: 'https://creadix-api.herokuapp.com/v1/api'
};
